from igraph import *


def find(graph, nodeName):
    return graph.vs.find(nodeName)
    
def degree(graph,node):
    return graph.degree(node)

def inDegree(graph,node):
    return graph.degree(node,mode="in")

def outDegree(graph,node):
    return graph.degree(node,mode="out")

def betweeness(graph,node):
    return graph.betweenness(node)

def transactionSaldo(graph,node):
    return 0 

"""
sum(graph.vs["weight"])
def transactionOut(graph,node):
    return ''  

def transactionIn(graph,node):
    return '' 
"""

def averagePathLength(graph):
    return graph.average_path_length(directed=True, unconn=True)

def networkDiameter(graph):
    return graph.diameter()

def getNodeCount(graph):
    return len(graph.vs)

def edgeCount(graph):
    return len(graph.es);

"""The vertex connectivity of a graph or two vertices,
 this is recently also called group cohesion."""
def connectivity(graph):
    return graph.vertex_connectivity();

def randomWalk(graph):
    return graph.community_walktrap(weights=None, steps=4)

def louvain(graph):
    return graph.community_multilevel(weights=None, return_levels=False)



"""degree_distribution"""

"""Transitivity measures the probability that the adjacent vertices of a vertex are connected. 
This is sometimes also called the clustering coefficient."""
def clusteringCoefficient(graph):
    return graph.transitivity_undirected(mode="nan")




