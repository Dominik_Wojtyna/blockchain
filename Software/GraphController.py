import GraphService as service

def getGraphInfo(graph):
    result={}
    result['nodeCount'] = service.getNodeCount(graph)
    result['edgeCount'] = service.edgeCount(graph)
    result['averagePathLength'] = service.averagePathLength(graph)
    result['networkDiameter'] = service.networkDiameter(graph)
    result['connectivity'] = service.connectivity(graph)
    result['clusteringCoefficient'] = service.clusteringCoefficient(graph)
    return result;    

def averagePathLength(graph):
    return service.averagePathLength(graph)

def networkDiameter(graph):
    return service.networkDiameter(graph)

def nodeCount(graph):
    return service.nodeCount(graph)

def edgeCount(graph):
    return service.nodeCount(graph)

def connectivity(graph):
    return service.connectivity(graph)

def clusteringCoefficient(graph):
    return service.clusteringCoefficient(graph)


