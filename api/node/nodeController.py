import node.nodeService as service
import block.blockService as blockService
from flask import Blueprint, request
from flasgger import swag_from

node_api = Blueprint('node_api', __name__)

@node_api.route('node_info', methods=['POST'])
@swag_from('nodeInfo.yml')
def nodeInfo():
    form = request.get_json() 
    graph = blockService.get_graph_from_edges(form['graph'])  
    nodeName = form['nodeName']
    result={}
    node = service.find(graph, nodeName)
    if (node is None):
        return result
    #result['inDegree'] = service.inDegree(graph, node)
    #result['outDegree'] = service.outDegree(graph, node)
    result['degree'] = service.degree(graph, node)
    result['betweeness'] = service.betweeness(graph, node)
    result['transactionSaldo'] = service.transactionSaldo(graph, node)
    return result;    

@node_api.route('', methods=['POST'])
@swag_from('find.yml')
def find():
    form = request.get_json() 
    graph = blockService.get_graph_from_edges(form['graph'])  
    nodeName = form['nodeName']
    return service.find(graph,nodeName)

@node_api.route('degree', methods=['POST'])
@swag_from('degree.yml')
def degree():
    form = request.get_json() 
    graph = blockService.get_graph_from_edges(form['graph'])  
    nodeName = form['nodeName']
    node = service.find(graph,nodeName)
    return service.degree(graph,node)

@node_api.route('betweeness', methods=['POST'])
@swag_from('betweeness.yml')
def betweeness():
    form = request.get_json() 
    graph = blockService.get_graph_from_edges(form['graph'])  
    nodeName = form['nodeName']
    node = service.find(graph, nodeName)
    return service.betweenness(graph, node)

