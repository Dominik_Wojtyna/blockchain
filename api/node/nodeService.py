from igraph import *


def find(graph, nodeName):
    """
    Return node from graph with given name
    Parameters:
         graph - Graph object on which method is perform
         nodeName - Name of node. In this case it's bitcoin address.
    Return:
        node
    """
    return graph.vs.find(nodeName)
    
def degree(graph, node, mode="all", loops=True):
    """
    Returns some vertex degrees from the graph.
    This method accepts a single node or a list of nodes as a parameter,     
    Parameters:
        graph - Graph object on which method is perform
        vertices - a single vertex ID or a list of vertex IDs
        mode - the type of degree to be returned (OUT for out-degrees, IN IN for in-degrees or ALL for the sum of them).
        loops - whether self-loops should be counted.
    Returns:
        the degree of the given nodes(in the form of a single integer or a list, depending on the input parameter).
   """
    return graph.degree(node, mode, loops)

def betweeness(graph, node, directed=True, cutoff=None, weights=None, nobigint=False):
    """
    Calculates or estimates the betweenness of nodes in a graph.
    Parameters:
        graph - Graph object on which method is perform
        node - the nodes for which the betweennesses must be returned. If None, assumes all of the nofes in the graph.
        directed - whether to consider directed paths.
        cutoff - if it is an integer, only paths less than or equal to this length are considered, effectively resulting in an estimation of the betweenness for the given vertices. If None, the exact betweenness is returned.
        weights - edge weights to be used. Can be a sequence or iterable or even an edge attribute name.
        nobigint - if True, igraph uses the longest available integer type on the current platform to count shortest paths. For some large networks that have a specific structure, the counters may overflow. To prevent this, use nobigint=False, which forces igraph to use arbitrary precision integers at the expense of increased computation time.
    Returns:
        the (possibly estimated) betweenness of the given nodes in a list
    """
    return graph.betweenness(node, directed, cutoff, weights, nobigint)

