import os
from flask import Flask
from flasgger import Swagger
#from flask_pymongo import PyMongo

from communities.communityController import community_api
from graph.graphController import graph_api
from node.nodeController import node_api
from block.blockController import block_api

app = Flask(__name__)


app.register_blueprint(community_api, url_prefix='/community')
app.register_blueprint(graph_api, url_prefix='/graph')
app.register_blueprint(node_api, url_prefix='/node')
app.register_blueprint(block_api, url_prefix='/block')

#app.config['MONGO_HOST'] = 'btc-blockchain-db'
#app.config['MONGO_USERNAME'] = os.environ['MONGODB_READONLY_USER']
#app.config['MONGO_PASSWORD'] = os.environ['MONGODB_READONLY_PASS']
#app.config['MONGO_DBNAME'] = 'bitcoin'

#mongo = PyMongo(app)
@app.after_request
def allow_origin(response):
    response.headers['Access-Control-Allow-Origin'] = 'http://example.com'
    response.headers['Access-Control-Allow-Credentials'] = 'true'

    return response


app.config['SWAGGER'] = {
    "swagger_version": "2.0",
    "title": "API docs",
    "headers": [
        ('Access-Control-Allow-Origin', '*'),
        ('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE, OPTIONS"),
        ('Access-Control-Allow-Credentials', "true"),
    ],
    "specs": [
        {
            "version": "1.0.0",
            "title": "Blockchain Analysis Platform API",
            "endpoint": 'api',
            "description": 'Description',
            "route": '/api'        
        }
    ]
}

Swagger(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=8000)
