from igraph import Graph, STRONG
from lastblock import lastBlock
from collections import defaultdict

    
def getGraph():

    blocks = get_block()
    edges = get_edges_from_blocks(blocks)
    graph = get_graph_from_edges(edges)
    return graph

def count(db, height_from, height_to):
    blocks = get_blocks(db, height_from, height_to)
    edges = get_edges_from_blocks(blocks)
    graph = get_graph_from_edges(edges)
    v_counts = get_separate_graphs_counts(graph)
    v_counts.sort(reverse=True)
    return v_counts

def get_edges_from_blocks(blocks):
    edges = []
    for block in blocks:
        for tx in block['transactions']:
            for inpt in tx['inputs']:
                for inp_addr in inpt['addresses']:
                    for outpt in tx['outputs']:
                        for out_addr in outpt['addresses']:
                            if None not in (inp_addr, out_addr):
                                edges.append((
                                    inp_addr['address'],
                                    out_addr['address'],
                                ))
    return edges


def get_graph_from_edges(edges):
    graph = Graph()
    for edge in edges:
        for vertex in edge:
            graph.add_vertex(name=vertex)
        graph.add_edge(edge[0], edge[1])
    return graph

def get_block():
    return [lastBlock()]

def get_values(blocks):
    edges = []
    result= defaultdict(int)
    for block in blocks:
        for tx in block['transactions']:
                    for outpt in tx['outputs']:
			if outpt.value != 0:
			    address= outpt['addresses'][0]
		            result[address]+=outpt.value
    return edges


