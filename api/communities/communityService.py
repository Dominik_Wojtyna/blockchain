from igraph import *

def randomWalk(graph,weights=None, steps=4):
    """
    Community detection algorithm of Latapy & Pons, based on random walks.
    Parameters:
        graph - Graph object on which method is perform
        weights - name of an edge attribute or a list containing edge weights. 
        steps - length of random walks to perform               
    Returns:
        VertexDendrogram object, initially cut at the maximum modularity.
       
    """
    return graph.community_walktrap(weights, steps).subgraphs()

def louvain(graph, weights=None, return_levels=False):
    """
    Community structure based on the multilevel algorithm of Blondel et al.
    This algorithm is said to run almost in linear time on sparse graphs.
    Parameters:
        graph - Graph object on which method is perform  
        weights - edge attribute name or a list containing edge weights
        return_levels - if True, the communities at each level are returned in a list. If False, only the community structure with the best modularity is returned.
    Returns:
        VertexClustering corresponding to the best modularity.      
    """
    return graph.community_multilevel(weights, return_levels).subgraphs()


