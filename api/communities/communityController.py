import communities.communityService as communityService
import block.blockService as blockService
from flask import Blueprint, request, jsonify
from flasgger import swag_from
import sys

community_api = Blueprint('community_api', __name__)


@community_api.route('random_walk', methods=['POST'])
@swag_from('randomWalk.yml')
def randomWalk():  
    form = request.get_json() 
    graph = blockService.get_graph_from_edges(form['graph'])  
    return jsonify(communityService.randomWalk(graph,form['steps']))


@community_api.route('louvain', methods=['POST'])
@swag_from('louvain.yml')
def louvain():
    form = request.get_json() 
    graph = blockService.get_graph_from_edges(form['graph'])
    return jsonify(communityService.louvain(graph))
