
def averagePathLength(graph, directed=True, unconn=True):
    """
    Community detection algorithm of Latapy & Pons, based on random walks.
    Calculates the average path length in a graph. 
    Parameters:
        graph - Graph object on which method is perform
        directed - whether to consider directed paths in case of a directed graph. Ignored for undirected graphs.
        unconn - what to do when the graph is unconnected. If True, the average of the geodesic lengths in the components is calculated. 
            Otherwise for all unconnected vertex pairs, a path length equal to the number of vertices is used.
    Returns:
            the average path length in the graph   
    """    
    return graph.average_path_length(directed, unconn)

def networkDiameter(graph, directed=True, unconn=True, weights=None):
    """
    Calculates the diameter of the graph (longest shortest path).
    Parameters:
        graph - Graph object on which method is perform
        directed - whether to consider directed paths.
        unconn - if True and the graph is unconnected, the longest geodesic within a component will be returned. 
            If False and the graph is unconnected, the result is the number of vertices if there are no weights or infinity if there are weights.
        weights - edge weights to be used. Can be a sequence or iterable or even an edge attribute name.
    Returns:
        the diameter (number)
    """
    return graph.diameter(directed, unconn, weights)

def nodeCount(graph):
    """
    Calculates number of nodes in graph
    Parameters:
        graph - Graph object on which method is perform
    Returns:
         nodes count (number)

    """
    return len(graph.vs)

def edgeCount(graph):
    """
    Calculates number of edges in graph
    Parameters:
        graph - Graph object on which method is perform
    Returns:
         edges count (number)

    """
    return len(graph.es);

def connectivity(graph, source=-1, target=-1, checks=True, neighbors="ignore"):
    """
    Calculates the vertex connectivity of the graph or between some vertices.
    The vertex connectivity between two given vertices is the number of vertices that have to be removed 
    in order to disconnect the two vertices into two separate components.
    The vertex connectivity of the graph is the minimal vertex connectivity over all vertex pairs.
    Parameters:
        graph - Graph object on which method is perform
        source - the source vertex involved in the calculation.
        target - the target vertex involved in the calculation.
        checks - if the whole graph connectivity is calculated and this is True, igraph performs some basic checks before calculation. 
        If the graph is not strongly connected, then the connectivity is obviously zero. If the minimum degree is one, then the connectivity is also one. 
        These simple checks are much faster than checking the entire graph, therefore it is advised to set this to True. The parameter is ignored if the connectivity between two given vertices is computed.
        neighbors - tells igraph what to do when the two vertices are connected. 
        "error" raises an exception, "infinity" returns infinity, "ignore" ignores the edge.
    Returns:
        the vertex connectivity
    """
    return graph.vertex_connectivity(source, target, checks, neighbors);



def clusteringCoefficient(graph,mode="zero"):
    """
    Calculates the global transitivity (clustering coefficient) of the graph.
    The transitivity measures the probability that two neighbors of a vertex are connected.     
    Parameters:
        graph - Graph object on which method is perform
        mode - if TRANSITIVITY_ZERO or "zero", the result will be zero if the graph does not have any triplets. If "nan" or TRANSITIVITY_NAN, the result will be NaN (not a number).
        Returns:
        the transitivity (number)
    """
    return graph.transitivity_undirected(mode)




