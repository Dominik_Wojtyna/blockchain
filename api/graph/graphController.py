import graph.graphService as service
import block.blockService as blockService
from flask import Blueprint, request, jsonify
from flasgger import swag_from

graph_api = Blueprint('graph_api', __name__)


@graph_api.route('graph_info', methods=['POST'])
@swag_from('graphInfo.yml')
def graphInfo():
    form = request.get_json()      
    graph = blockService.get_graph_from_edges(form['graph'])
    result = {}
    result['nodeCount'] = service.nodeCount(graph)
    result['edgeCount'] = service.edgeCount(graph)
    result['averagePathLength'] = service.averagePathLength(graph)
    result['networkDiameter'] = service.networkDiameter(graph)
    result['connectivity'] = service.connectivity(graph)
    result['clusteringCoefficient'] = service.clusteringCoefficient(graph)
    return jsonify(result)


@graph_api.route('average_path', methods=['POST'])
@swag_from('averagePathLength.yml')
def averagePathLength():   
    form = request.get_json()     
    graph = blockService.get_graph_from_edges(form['graph'])
    return jsonify(service.averagePathLength(graph))


@graph_api.route('network_diameter', methods=['POST'])
@swag_from('networkDiameter.yml')
def networkDiameter():
    form = request.get_json()      
    graph = blockService.get_graph_from_edges(form['graph'])
    return jsonify(service.networkDiameter(graph))


@graph_api.route('node_count', methods=['POST'])
@swag_from('nodeCount.yml')
def nodeCount():
    form = request.get_json()      
    graph = blockService.get_graph_from_edges(form['graph'])
    return jsonify(service.nodeCount(graph))


@graph_api.route('edge_count', methods=['POST'])
@swag_from('edgeCount.yml')
def edgeCount():
    form = request.get_json()      
    graph = blockService.get_graph_from_edges(form['graph'])
    return jsonify(service.nodeCount(graph))


@graph_api.route('connectivity', methods=['POST'])
@swag_from('connectivity.yml')
def connectivity():
    form = request.get_json()      
    graph = blockService.get_graph_from_edges(form['graph'])
    return jsonify(service.connectivity(graph))


@graph_api.route('clustering_coefficient', methods=['POST'])
@swag_from('clusteringCoefficient.yml')
def clusteringCoefficient():
    form = request.get_json()      
    graph = blockService.get_graph_from_edges(form['graph'])
    return jsonify(service.clusteringCoefficient(graph))


