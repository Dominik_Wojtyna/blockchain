\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1\relax .\kern .5em }Introduction}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1\relax .\kern .5em }Abstract}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2\relax .\kern .5em }Background}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3\relax .\kern .5em }Motivation}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4\relax .\kern .5em }Range}{4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5\relax .\kern .5em }Synopsis}{4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2\relax .\kern .5em }Literature review}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1\relax .\kern .5em }Introduction}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2\relax .\kern .5em }Overview}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3\relax .\kern .5em }Alternative software}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1\relax .\kern .5em }Block Explorers}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2\relax .\kern .5em }Block Databases}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3\relax .\kern .5em }Block analysis software}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4\relax .\kern .5em }Conclusion}{6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3\relax .\kern .5em }Theory introduction}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1\relax .\kern .5em }Introduction}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2\relax .\kern .5em }Bitcoin}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1\relax .\kern .5em }What is Bitcoin?}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2\relax .\kern .5em }Elements of Bitcoin}{7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3\relax .\kern .5em }How it works?}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3\relax .\kern .5em }Blockchain}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1\relax .\kern .5em }Introduction}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2\relax .\kern .5em }Fields of use}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3\relax .\kern .5em }Structure of blockchain}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.4\relax .\kern .5em }Single block structure}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4\relax .\kern .5em }Security}{9}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5\relax .\kern .5em }Graph theory}{10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6\relax .\kern .5em }Conclusion}{10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4\relax .\kern .5em }Aim}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1\relax .\kern .5em }Introduction}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2\relax .\kern .5em }Users}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3\relax .\kern .5em }Functional requirements}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 1}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 2}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 3}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 4}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 5}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 6}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 7}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 8}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{User Story 9}{11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4\relax .\kern .5em }Non-functional requirements}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1\relax .\kern .5em }Usability}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2\relax .\kern .5em }Security}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.3\relax .\kern .5em }Performance}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.4\relax .\kern .5em }Availability}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.5\relax .\kern .5em }Scalability}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5\relax .\kern .5em }Conclusion}{12}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5\relax .\kern .5em }Materials and Methods}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1\relax .\kern .5em }Introduction}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2\relax .\kern .5em }Database}{13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3\relax .\kern .5em }Metrics}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1\relax .\kern .5em }Graph}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2\relax .\kern .5em }Node}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.3\relax .\kern .5em }Clustering}{14}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4\relax .\kern .5em }Tools}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.1\relax .\kern .5em }Python}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.2\relax .\kern .5em }Git}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.3\relax .\kern .5em }IGraph}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.4\relax .\kern .5em }Flask}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.5\relax .\kern .5em }Swagger}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.6\relax .\kern .5em }Flasgger}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.7\relax .\kern .5em }REST}{16}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.8\relax .\kern .5em }JSON}{16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5\relax .\kern .5em }Conclusion}{17}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6\relax .\kern .5em }Implementation}{18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1\relax .\kern .5em }Introduction}{18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2\relax .\kern .5em }API usage}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.1\relax .\kern .5em }API}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.2\relax .\kern .5em }Project organization}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.3\relax .\kern .5em }Layered Architecture}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.4\relax .\kern .5em }Model}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.5\relax .\kern .5em }Documentation}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.6\relax .\kern .5em }YAML}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.7\relax .\kern .5em }Using API Example}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.8\relax .\kern .5em }Requirments}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.9\relax .\kern .5em }View}{20}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3\relax .\kern .5em }Conclusion}{20}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7\relax .\kern .5em }Results}{21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1\relax .\kern .5em }Introduction}{21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2\relax .\kern .5em }Functional benchmark}{21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3\relax .\kern .5em }Conclusion}{21}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8\relax .\kern .5em }Summary}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1\relax .\kern .5em }Introduction}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2\relax .\kern .5em }Requirements review}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3\relax .\kern .5em }Problems}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.4\relax .\kern .5em }Future directions}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.5\relax .\kern .5em }Conclusions}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.6\relax .\kern .5em }Bibliography}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.7\relax .\kern .5em }List of figures}{22}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Figures}{23}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.8\relax .\kern .5em }List of tables}{23}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{List of Tables}{24}
